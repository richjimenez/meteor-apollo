import { ApolloServer, gql } from 'apollo-server-express';
import { WebApp } from 'meteor/webapp';
import { getUser } from 'meteor/apollo';
import merge from 'lodash/merge';

import ResolutionSchema from '/imports/api/resolutions/resolutions.graphql';
import ResolutionsResolvers from '/imports/api/resolutions/resolvers';

import TestSchema from '/imports/api/schema';
import TestResolver from '/imports/api/resolvers';

const typeDefs = [TestSchema, ResolutionSchema];
const resolvers = merge(TestResolver, ResolutionsResolvers);

const server = new ApolloServer({
  typeDefs,
  resolvers,
  // context: async ({ req }) => ({
  //   user: await getUser(req.headers.authorization),
  // }),
});

server.applyMiddleware({
  app: WebApp.connectHandlers,
  path: '/graphql',
});

WebApp.connectHandlers.use('/graphql', (req, res) => {
  if (req.method === 'GET') {
    res.end();
  }
});
