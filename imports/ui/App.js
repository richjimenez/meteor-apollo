import React from 'react';
import { gql } from 'apollo-boost';
import { Query } from 'react-apollo';

const GetData = gql`
  {
    hi
    resolutions {
      _id
      name
    }
  }
`;

const App = () => (
  <Query query={GetData}>
    {({ loading, error, data }) => {
      if (loading) return <div>Loading...</div>;
      if (error) return <div>Error :(</div>;

      return (
        <div>
          <h1>{data.hi}</h1>
          <ul>
            {data.resolutions.map(e => (
              <li key={e._id}>{e.name}</li>
            ))}
          </ul>
        </div>
      );
    }}
  </Query>
);

export default App;
