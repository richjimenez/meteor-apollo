import Resolutions from './resolutions';

export default {
  Query: {
    resolutions() {
      return Resolutions.find().fetch();
    },
  },
};
